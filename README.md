# Entity Markup
Allows control over how elements are rendered for view modes.

### How to use
The module has the following dependencies:   
- field

Once the module is enabled, a new 'Manage markup' option will be added to all entities. The available modes mimic whatever is defined for view modes for the entity.
Available options are determined by the cardinality of the field.
