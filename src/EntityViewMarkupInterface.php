<?php

namespace Drupal\entity_markup;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides a common interface for entity displays.
 */
interface EntityViewMarkupInterface extends ConfigEntityInterface {

  /**
   * Gets the entity type for which this display is used.
   *
   * @return string
   *   The entity type id.
   */
  public function getTargetEntityTypeId();

  /**
   * Gets the view or form mode to be displayed.
   *
   * @return string
   *   The mode to be displayed.
   */
  public function getMode();


  /**
   * Gets the bundle to be displayed.
   *
   * @return string
   *   The bundle to be displayed.
   */
  public function getTargetBundle();

}