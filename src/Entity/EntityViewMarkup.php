<?php

namespace Drupal\entity_markup\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\entity_markup\EntityViewMarkupInterface;

/**
 * Configuration entity that contains display options for all components of a
 * rendered entity in a given view mode.
 *
 * @ConfigEntityType(
 *   id = "entity_view_markup",
 *   label = @Translation("Entity view markup"),
 *   entity_keys = {
 *     "id" = "id"
 *   },
 *   handlers = {
 *     "access" =
 *   "\Drupal\Core\Entity\Entity\Access\EntityViewDisplayAccessControlHandler",
 *   },
 *   config_export = {
 *     "id",
 *     "targetEntityType",
 *     "bundle",
 *     "mode",
 *     "field_markup",
 *   }
 * )
 */
class EntityViewMarkup extends ConfigEntityBase implements EntityViewMarkupInterface {

  /**
   * Unique ID for the config entity.
   *
   * @var string
   */
  protected $id;

  /**
   * Entity type to be displayed.
   *
   * @var string
   */
  protected $targetEntityType;

  /**
   * Bundle to be displayed.
   *
   * @var string
   */
  protected $bundle;

  /**
   * View or form mode to be displayed.
   *
   * @var string
   */
  protected $mode;

  /**
   * List of field markup options, keyed by component name.
   *
   * @var array
   */
  protected $field_markup = [];


  /**
   * {@inheritdoc}
   */
  public function getTargetEntityTypeId() {
    return $this->targetEntityType;
  }

  /**
   * {@inheritdoc}
   */
  public function getMode() {
    return $this->get('mode');
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetBundle() {
    return $this->bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->targetEntityType . '.' . $this->bundle . '.' . $this->mode;
  }

}