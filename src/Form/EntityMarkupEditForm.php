<?php

namespace Drupal\entity_markup\Form;

use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\entity_markup\Entity\EntityViewMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Edit form for the EntityViewDisplay entity type.
 *
 * @internal
 */
class EntityMarkupEditForm extends FormBase {

  /**
   * The entity type manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity view display.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected $entityDisplay;
  
  /**
   * Constructs a new EntityMarkupEditForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, RouteMatchInterface $route_match) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->routeMatch = $route_match;

    $route_parameters = $route_match->getParameters()->all();

    $this->entityDisplay = $entity_type_manager
      ->getStorage('entity_view_display')
      ->load($route_parameters['entity_type_id'] . '.' . $route_parameters['bundle'] . '.' . $route_parameters['view_mode_name']);

    if ($this->entityDisplay === NULL) {
      $values = [
        'targetEntityType' => $route_parameters['entity_type_id'],
        'bundle' => $route_parameters['bundle'],
        'mode' => $route_parameters['view_mode_name'],
        'status' => TRUE,
      ];
      $this->entityDisplay = $entity_type_manager
        ->getStorage('entity_view_display')
        ->create($values);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('current_route_match')
    );
  }

  public function buildForm(array $form, FormStateInterface $form_state, ContentEntityTypeInterface $entity_type = NULL) {
    $form = [];
    $route_parameters = $this->routeMatch->getParameters()->all();
    // Try loading the view markup from configuration.
    $config = EntityViewMarkup::load($route_parameters['entity_type_id'] . '.' . $route_parameters['bundle'] . '.' . $route_parameters['view_mode_name']);

    // If not found, create a fresh view markup object. We do not preemptively create
    // new entity_view_markup configuration entries for each existing entity type
    // and bundle whenever a new view mode becomes available. Instead,
    // configuration entries are only created when a view markup object is explicitly
    // configured and saved.
    if (!$config) {
      $config = EntityViewMarkup::create([
        'targetEntityType' => $route_parameters['entity_type_id'],
        'bundle' => $route_parameters['bundle'],
        'mode' => $route_parameters['view_mode_name'],
      ]);
    }

    $is_layout_builder = $this->entityDisplay->getThirdPartySetting('layout_builder', 'enabled');
    $all_field_definitions = $this->entityFieldManager->getFieldDefinitions($route_parameters['entity_type_id'], $route_parameters['bundle']);
    $field_definitions = array_filter($all_field_definitions, function (FieldDefinitionInterface $field_definition) use ($is_layout_builder) {
      $field_name = $field_definition->getName();
      $display_options = $this->entityDisplay->getComponent($field_name);
      $in_visible_region = isset($display_options) && isset($display_options['region']) && $display_options['region'] != 'hidden';
      $is_title = $field_name == 'title';
      $supports_view = $field_definition->isDisplayConfigurable('view') && !in_array($field_name, ['layout_builder__layout', 'langcode']) && $field_definition->getType() != 'metatag';
      return $is_title || $supports_view && ($in_visible_region || (isset($is_layout_builder) && $is_layout_builder == TRUE));
    });

    $table = [
      '#type' => 'table',
      '#header' => [
        ['data' => $this->t('Field')],
        ['data' => $this->t('Display label')],
        ['data' => $this->t('Label tag')],
        ['data' => $this->t('Field tag')],
        ['data' => $this->t('Field items tag')],
        ['data' => $this->t('Field item tag')],
        ['data' => $this->t('Additional classes')],
        ['data' => $this->t('Additional item classes')],
      ],
    ];

    $field_markup = $config->get('field_markup');
    // Field rows.
    foreach ($field_definitions as $field_name => $field_definition) {
      $table[$field_name]['field'] = ['#plain_text' => $field_definition->getLabel()];
      $display_label = NULL;
      $label_tag= NULL;
      $field_tag = NULL;
      $field_items_tag = NULL;
      $field_item_tag = NULL;
      $field_classes = NULL;
      $field_item_classes = NULL;
      if (isset($field_markup[$field_name])) {
        if (isset($field_markup[$field_name]['display_label'])) {
          $display_label = $field_markup[$field_name]['display_label'];
        }
        if (isset($field_markup[$field_name]['label_tag'])) {
          $label_tag = $field_markup[$field_name]['label_tag'];
        }
        if (isset($field_markup[$field_name]['field_tag'])) {
          $field_tag = $field_markup[$field_name]['field_tag'];
        }
        if (isset($field_markup[$field_name]['field_items_tag'])) {
          $field_items_tag = $field_markup[$field_name]['field_items_tag'];
        }
        if (isset($field_markup[$field_name]['field_item_tag'])) {
          $field_item_tag = $field_markup[$field_name]['field_item_tag'];
        }
        if (isset($field_markup[$field_name]['field_classes'])) {
          $field_classes = $field_markup[$field_name]['field_classes'];
        }
        if (isset($field_markup[$field_name]['field_item_classes'])) {
          $field_item_classes = $field_markup[$field_name]['field_item_classes'];
        }
      }
      $table[$field_name]['display_label'] = $this->buildLabelText($field_definition, $display_label);
      $table[$field_name]['label_tag'] = $this->buildLabelMarkupDropdown($field_definition, $label_tag);
      $table[$field_name]['field_tag'] = $this->buildFieldMarkupDropdown($field_definition, $field_tag);
      $table[$field_name]['field_items_tag'] = $this->buildFieldItemsMarkupDropdown($field_definition, $field_items_tag);
      $table[$field_name]['field_item_tag'] = $this->buildFieldItemMarkupDropdown($field_definition, $field_item_tag);
      $table[$field_name]['field_classes'] = $this->buildAdditionalClasses($field_definition, $field_classes);
      $table[$field_name]['field_item_classes'] = $this->buildAdditionalItemClasses($field_definition, $field_item_classes);
    }

    $form['description'] = [
      '#markup' => '<p>Overriden fields may not accept the parameters set here unless they extend from one of the base entity markup twig files.</p>',
    ];

    $form['field_list'] = $table;
    $form['entity_type_id'] = [
      '#type' => 'hidden',
      '#value' => $route_parameters['entity_type_id'],
    ];
    $form['bundle'] = [
      '#type' => 'hidden',
      '#value' => $route_parameters['bundle'],
    ];
    $form['view_mode_name'] = [
      '#type' => 'hidden',
      '#value' => $route_parameters['view_mode_name'],
    ];
    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  public function buildLabelText(FieldDefinitionInterface $field_definition, $default_value = NULL) {
    $display_options = $this->entityDisplay->getComponent($field_definition->getName());
    if (!isset($display_options) || $display_options['label'] != "hidden") {
      return [
        '#type' => 'textfield',
        '#default_value' => $default_value,
        '#placeholder' => $field_definition->getLabel(),
        '#attributes' => ['style' => 'width:80%;'],
      ];
    }

    return [];
  }

  public function buildLabelMarkupDropdown(FieldDefinitionInterface $field_definition, $default_value = NULL) {
    $display_options = $this->entityDisplay->getComponent($field_definition->getName());
    if (!isset($display_options) || $display_options['label'] != "hidden") {
      return [
        '#type' => 'select',
        '#default_value' => $default_value,
        '#options' => [
          '' => $this->t('Default'),
          'figure' => $this->t('Figure'),
          'p' => $this->t('Paragraph'),
          'span' => $this->t('Span'),
          'h1' => $this->t('Heading 1'),
          'h2' => $this->t('Heading 2'),
          'h3' => $this->t('Heading 3'),
          'h4' => $this->t('Heading 4'),
          'h5' => $this->t('Heading 5'),
          'h6' => $this->t('Heading 6'),
        ],
      ];
    }
    return [];
  }

  public function buildFieldMarkupDropdown(FieldDefinitionInterface $field_definition, $default_value = NULL) {
    $display_options = $this->entityDisplay->getComponent($field_definition->getName());
    if (!isset($display_options) || $display_options['label'] != "hidden") {
      return [
        '#type' => 'select',
        '#default_value' => $default_value,
        '#options' => [
          '' => $this->t('Default'),
          'remove' => '- Remove -',
          'figure' => $this->t('Figure'),
          'p' => $this->t('Paragraph'),
          'ul' => $this->t('Unordered List'),
        ],
      ];
    }

    return [];
  }

  public function buildFieldItemsMarkupDropdown(FieldDefinitionInterface $field_definition, $default_value = NULL) {
    $field_storage = $field_definition->getFieldStorageDefinition();
    if ($field_storage->getCardinality() != 1) {
      return [
        '#type' => 'select',
        '#default_value' => $default_value,
        '#options' => [
          '' => $this->t('Default'),
          'remove' => '- Remove -',
          'figure' => $this->t('Figure'),
          'p' => $this->t('Paragraph'),
          'ul' => $this->t('Unordered List'),
        ],
      ];
    }

    return [];
  }

  public function buildFieldItemMarkupDropdown(FieldDefinitionInterface $field_definition, $default_value = NULL) {
    return [
      '#type' => 'select',
      '#default_value' => $default_value,
      '#options' => [
        '' => $this->t('Default'),
        'remove' => '- Remove -',
        'figure' => $this->t('Figure'),
        'p' => $this->t('Paragraph'),
        'span' => $this->t('Span'),
        'li' => $this->t('List item'),
        'h1' => $this->t('Heading 1'),
        'h2' => $this->t('Heading 2'),
        'h3' => $this->t('Heading 3'),
        'h4' => $this->t('Heading 4'),
        'h5' => $this->t('Heading 5'),
        'h6' => $this->t('Heading 6'),
      ],
    ];
  }

  public function buildAdditionalClasses(FieldDefinitionInterface $field_definition, $default_value = NULL) {
    $classes_field = [
      '#type' => 'textfield',
      '#default_value' => $default_value,
      '#attributes' => ['style' => 'width:80%;'],
    ];

    if ($field_definition instanceof ThirdPartySettingsInterface) {
      $definition = $field_definition->getThirdPartySetting('entity_markup', 'field_classes');
      if (isset($definition) && !empty($definition)) {
        $classes_field['#placeholder'] = $definition;
      }
    }
    return $classes_field;
  }

  public function buildAdditionalItemClasses(FieldDefinitionInterface $field_definition, $default_value = NULL) {
    $classes_field = [
      '#type' => 'textfield',
      '#default_value' => $default_value,
      '#attributes' => ['style' => 'width:80%;'],
    ];

    if ($field_definition instanceof ThirdPartySettingsInterface) {
      $definition = $field_definition->getThirdPartySetting('entity_markup', 'field_item_classes');
      if (isset($definition) && !empty($definition)) {
        $classes_field['#placeholder'] = $definition;
      }
    }
    return $classes_field;
  }

  public function getFormId() {
    $route_parameters = $this->routeMatch->getParameters()->all();
    return 'entity_markup_' . $route_parameters['entity_type_id'] . '_' . $route_parameters['bundle'] . '_' . $route_parameters['view_mode_name'];
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Try loading the view markup from configuration.
    $config = EntityViewMarkup::load($values['entity_type_id'] . '.' . $values['bundle'] . '.' . $values['view_mode_name']);

    // If not found, create a fresh view markup object. We do not preemptively create
    // new entity_view_markup configuration entries for each existing entity type
    // and bundle whenever a new view mode becomes available. Instead,
    // configuration entries are only created when a view markup object is explicitly
    // configured and saved.
    if (!$config) {
      $config = EntityViewMarkup::create([
        'targetEntityType' => $values['entity_type_id'],
        'bundle' => $values['bundle'],
        'mode' => $values['view_mode_name'],
      ]);
    }
    $config->set('targetEntityType', $values['entity_type_id']);
    $config->set('bundle', $values['bundle']);
    $config->set('mode', $values['view_mode_name']);

    $field_markup = [];
    foreach ($values['field_list'] as $field_name => $field_values) {
      foreach ($field_values as $key => $value) {
        if (!empty($value)) {
          $field_markup[$field_name][$key] = $value;
        }

      }
    }

    if (!empty($field_markup)) {
      $config->set('field_markup', $field_markup);
      $config->save();
    }
    else {
      $config->delete();
    }

    $this->messenger()->addStatus('Your settings have been saved.');
  }

}
